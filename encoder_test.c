/*
 *    encoder_test/encoder_test.c created by makepico
 */


#include "encoder_test.h"

uint32_t get_encoder(PIO pio, uint sm) {
    uint32_t rotator=0;
    int32_t buffer= pio_sm_get_rx_fifo_level(pio, sm);
    if (buffer>0){
        rotator = pio_sm_get(pio, sm);
        printf("%d : %u\n", buffer, rotator);
    }
    return rotator;
}

const int PIN_RX = 2;

int main() {
    stdio_init_all();
        // todo get free sm
    PIO pio = pio0;
    int sm = 0;
    uint offset = pio_add_program(pio, &encoder_test_program);

    encoder_test_init(pio, sm, offset, PIN_RX, 2);

    while (1){
       // printf("%u\n", get_encoder(pio, sm));
       get_encoder(pio, sm);
        //puts("hello");
        //sleep_ms(500);
    }
    return 0;
}


/*
static inline void put_pixel(uint32_t pixel_grb) {
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}
*/
